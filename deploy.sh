#!/bin/bash
set -e

cd ../qalorie
./push-google.sh

cd ../qalorie-kube
kubectl delete -f qalorie.yaml 
echo "Waiting for pod to get deleted..."
sleep 5
while [ true ]; do
  result=`kubectl get pods`
  if [[ $result == *"qalorie"* ]]; then
    sleep 1
  else
    break
  fi
done
kubectl create -f qalorie.yaml 
